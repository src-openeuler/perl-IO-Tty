Name:           perl-IO-Tty
Version:        1.20
Release:        2
Summary:        A interface to pseudo tty's for perl
License:        (GPL-1.0-or-later OR Artistic-1.0-Perl) AND BSD-2-Clause
URL:            https://metacpan.org/release/IO-Tty
Source0:        https://cpan.metacpan.org/authors/id/T/TO/TODDR/IO-Tty-%{version}.tar.gz
BuildRequires:  coreutils findutils make perl-interpreter perl-devel perl-generators
BuildRequires:  perl(Config) perl(Cwd) perl(Exporter) perl(ExtUtils::MakeMaker)
BuildRequires:  gcc perl(Carp) perl(DynaLoader) perl(IO::File) perl(IO::Handle) perl(POSIX)
BuildRequires:  perl(strict) perl(vars) perl(Test::More) perl(warnings)

%{?perl_default_filter}

%description
Supply an interface to pseudo tty's with IO::Tty and IO::Pty.

%package_help

%prep
%autosetup -n IO-Tty-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}

%check
make test

%files
%doc ChangeLog README
%{perl_vendorarch}/auto/IO/
%{perl_vendorarch}/IO/

%files help
%{_mandir}/man3/{IO::Pty.3*,IO::Tty.3*,IO::Tty::Constant.3*}

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 1.20-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jul 23 2024 gss <guoshengsheng@kylinos.cn> - 1.20-1
- Upgrade to version 1.20
- Skip t/pty_get_winsize.t tests on AIX
- Fix patchlevel check for util.h
- Remove --no-undefined from compiler test which is not compatible with all platforms.

* Sat Sep 09 2023 wangkai <13474090681@163.com> - 1.17-1
- Upgrade to version 1.17

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.16-1
- Upgrade to version 1.16

* Wed Jun 02 2021 zhaoyao<zhaoyao32@huawei.com> - 1.12-15
- fixs faileds: /bin/sh: gcc: command not found.

* Wed Feb 19 2020 wutao <wutao61@huawei.com> - 1.12-14
- Package init
